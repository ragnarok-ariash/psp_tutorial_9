package com.example.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.CourseModel;
import com.example.service.CourseService;

@RestController
@RequestMapping("/rest")
public class CourseRestController {
	@Autowired
	CourseService courseService;
	
	@RequestMapping("/course/view/{id}")
	public CourseModel viewPath(@PathVariable(value = "id") String courseId) {
		return courseService.selectCourse(courseId);
	}

	@RequestMapping("/course/viewall")
	public List<CourseModel> view() {
		return courseService.selectAllCourses();
	}

}
