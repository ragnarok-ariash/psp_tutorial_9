package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.model.CourseModel;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CourseServiceRest implements CourseService{
	@Autowired
	private CourseDAO courseDAO;

	@Override
	public CourseModel selectCourse(String courseId) {
		log.info ("REST - select course with courseId {}", courseId);
		
		return courseDAO.selectCourse(courseId);
	}

	@Override
	public List<CourseModel> selectAllCourses() {
		log.info ("REST - select all courses");
		
		return courseDAO.selectAllCourses();
	}

	@Override
	public void addCourse(CourseModel newCourse) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteCourse(String courseId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateCourse(CourseModel courseModel) {
		// TODO Auto-generated method stub
		
	}

}
