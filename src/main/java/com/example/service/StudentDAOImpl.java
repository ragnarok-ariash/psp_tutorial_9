package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.model.StudentModel;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class StudentDAOImpl implements StudentDAO{
	@Autowired
	private RestTemplate restTemplate;

	@Override
	public StudentModel selectStudent(String npm) {
		log.info ("get student with npm {}", npm);
		StudentModel student =
				restTemplate.getForObject(
				"http://localhost:8080/rest/student/view/"+npm,
				StudentModel.class);
		
		return student;
	}

	@Override
	public List<StudentModel> selectAllStudents() {
		log.info ("get all students");
		String apiUrl = "http://localhost:8080/rest/student/viewall";
		List<StudentModel> students = restTemplate.getForObject(apiUrl, List.class);
		return students;
	}

}
