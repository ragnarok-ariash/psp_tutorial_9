package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.model.CourseModel;

@Service
public class CourseDAOImpl implements CourseDAO {
	@Autowired
	private RestTemplate restTemplate;

	@Override
	public CourseModel selectCourse(String courseId) {
		CourseModel course =
				restTemplate.getForObject(
				"http://localhost:8080/rest/course/view/"+courseId,
				CourseModel.class);
		
		return course;
	}

	@Override
	public List<CourseModel> selectAllCourses() {
		String apiUrl = "http://localhost:8080/rest/course/viewall";
		List<CourseModel> courses = restTemplate.getForObject(apiUrl, List.class);
		return courses;
	}

}
