package com.example.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.example.model.CourseModel;
import com.example.model.StudentModel;

@Mapper
public interface CourseMapper {
	@Select("select * from course where id_course = #{courseId}")
	@Results(value = {
			@Result(property="idCourse", column="id_course"),
			@Result(property="nama", column="nama"),
			@Result(property="sks", column="sks"),
			@Result(property="students", column="id_course", 
			javaType = List.class, many = @Many(select="selectStudents"))
	})
	CourseModel selectCourse(@Param("courseId") String courseId);

	@Select("select * from course")
	@Results(value = {
			@Result(property="idCourse", column="id_course"),
			@Result(property="nama", column="nama"),
			@Result(property="sks", column="sks"),
			@Result(property="students", column="id_course", 
			javaType = List.class, many = @Many(select="selectStudents"))
	})
	List<CourseModel> selectAllCourses();

	@Insert("INSERT INTO course (id_course, nama, sks) VALUES (#{idCourse}, #{nama}, #{sks})")
	void addCourse(CourseModel newCourse);

	@Delete("Delete from course where id_course = #{courseId}")
	void deleteCourse(@Param("courseId") String courseId);

	@Update("Update course set nama = #{course.nama}, sks = #{course.sks}"
			+ " where id_course = #{course.idCourse}")
	void updateCourse(@Param("course") CourseModel courseModel);

	@Select("select student.npm, name, gpa "
	+"from studentcourse join student "
	+"on studentcourse.npm = student.npm "
	+"where studentcourse.id_course=#{courseId}")
	List<StudentModel> selectStudents(@Param("courseId") String courseId);
}
